package tech.niceprice.authorization.entity

import org.apache.ibatis.type.Alias
import org.springframework.security.oauth2.provider.client.BaseClientDetails

@Alias(value = "systemClientDetails")
data class SystemClientDetails(
    private val id: String,
    private val secret: String,
    private val resourceIds: String
): BaseClientDetails(
    id, resourceIds,
    "all",
    "client_credentials,refresh_token",
    "ROLE_CLIENT"
) {
    init {
        super.setClientSecret(secret)
    }
}