package tech.niceprice.authorization.entity

import org.apache.ibatis.type.Alias

@Alias(value = "groupDO")
data class GroupDO(
    var id: Long = 0,
    var groupName: String = "",
    var authorities: List<String> = listOf(),
    var members: List<Long> = listOf()
)