package tech.niceprice.authorization.entity

import org.apache.ibatis.type.Alias

@Alias(value = "userDO")
data class UserDO(
    var username: Long = 0,
    var password: String = "",
    var authorities: List<String> = listOf(),
    var enabled: Boolean = true
)