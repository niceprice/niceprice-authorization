package tech.niceprice.authorization.snowflake

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class SnowFlakeWorker(
    @Value(value = "\${snowflake.work-id}")
    private val workerId: Long, // 工作机器ID(0~31)
    @Value(value = "\${snowflake.data-center-id}")
    private val dataCenterId: Long, // 数据中心ID(0~31)
    @Value(value = "\${snowflake.start-timestamp}")
    private val startTimestamp: Long /* 开始时间截 (2020-01-01) */
) {
    companion object {
        /* 机器id所占的位数 */ private const val workerIdBits = 5
        /* 数据标识id所占的位数 */ private const val dataCenterIdBits = 5
        /* 支持的最大机器id 31 */ private const val maxWorkerId = -1L xor (-1L shl workerIdBits)
        /* 支持的最大数据标识id 31 */ private const val maxDataCenterId = -1L xor (-1L shl dataCenterIdBits)
        /* 序列在id中占的位数 */ private const val sequenceBits = 12
        /* 机器ID向左移12位 */private const val workerIdShift = sequenceBits
        /* 数据标识id左移17位 */private const val dataCenterIdShift = sequenceBits + workerIdBits
        /* 时间截左移22位 */private const val timestampLeftShift = sequenceBits + workerIdBits + dataCenterIdBits
        /* 生成序列掩码 4095 */ private const val sequenceMask = -1L xor (-1L shl sequenceBits)
        /* 毫秒内序列(0~4095) */private var sequence = 0L
        /* 上次生成ID的时间截 */private var lastTimestamp = -1L
    }

    init {
        require(!(workerId > maxWorkerId || workerId < 0)) {
            "Worker Id can't be greater than $maxWorkerId or less than 0"
        }
        require(!(dataCenterId > maxDataCenterId || dataCenterId < 0)) {
            "Data Center Id can't be greater than $maxDataCenterId or less than 0"
        }
    }

    /**
     * 获得下一个ID (该方法是线程安全的)
     * @return SnowflakeId
     */
    @Synchronized fun nextId(): Long {
        var timestamp = timeGen()
        //如果当前时间小于上一次ID生成的时间戳，说明系统时钟回退过这个时候应当抛出异常
        if (timestamp < lastTimestamp) throw RuntimeException(
                "Clock moved backwards. Refusing to generate id for ${lastTimestamp - timestamp} milliseconds")
        //如果是同一时间生成的，则进行毫秒内序列
        if (lastTimestamp == timestamp) {
            sequence = sequence + 1 and sequenceMask
            //毫秒内序列溢出, 阻塞到下一个毫秒,获得新的时间戳
            if (sequence == 0L) timestamp = tilNextMillis(lastTimestamp)
        } else sequence = 0L
        //时间戳改变，毫秒内序列重置
        //上次生成ID的时间截
        lastTimestamp = timestamp
        //移位并通过或运算拼到一起组成64位的ID
        return (timestamp - startTimestamp shl timestampLeftShift
            or (dataCenterId shl dataCenterIdShift)
            or (workerId shl workerIdShift)
            or sequence)
    }

    /**
     * 阻塞到下一个毫秒，直到获得新的时间戳
     * @param lastTimestamp 上次生成ID的时间截
     * @return 当前时间戳
     */
    private fun tilNextMillis(lastTimestamp: Long): Long {
        while (timeGen() <= lastTimestamp) continue
        return timeGen()
    }

     // 返回以毫秒为单位的当前时间
    private fun timeGen(): Long = System.currentTimeMillis()
}