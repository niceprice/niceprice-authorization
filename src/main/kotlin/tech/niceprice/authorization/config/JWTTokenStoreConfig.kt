package tech.niceprice.authorization.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.provider.token.DefaultTokenServices
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore

/**
 * 配置类
 * 管理 JWT 加密解密
 */
@Configuration
class JWTTokenStoreConfig(
    @Value(value = "\${security.oauth2.resource.jwt.key-value}")
    private val jwtKeyValue: String
) {
    @Bean
    fun tokenStore(
        converter: JwtAccessTokenConverter
    ): TokenStore = JwtTokenStore(converter)

    /**
     * spring security & oauth2 共用编码器
     * 在 WebSecurity 中注入
     * 为单例，且防止循环引用，在此处声明
     */
    @Bean fun passwordEncoder(): PasswordEncoder =
        PasswordEncoderFactories.createDelegatingPasswordEncoder()

    @Bean @Primary
    fun tokenServices(
        tokenStore: TokenStore
    ): DefaultTokenServices {
        val defaultTokenServices = DefaultTokenServices()
        defaultTokenServices.setTokenStore(tokenStore)
        defaultTokenServices.setSupportRefreshToken(true)
        return defaultTokenServices
    }

    @Bean @Primary
    fun jwtAccessTokenConverter(): JwtAccessTokenConverter {
        val jwt = JwtAccessTokenConverter()
        jwt.setSigningKey(jwtKeyValue)
        return jwt
    }
}