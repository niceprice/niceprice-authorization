package tech.niceprice.authorization.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import javax.sql.DataSource

/**
 * 配置类
 * 管理授权服务器
 * 声明 client_id, client_secret 、授权类型、scope 等
 * 配置授权令牌的加密解密链、转换器等
 * 配合 JWTTokenStoreConfig 使用
 */
@Configuration
@EnableAuthorizationServer
class AuthorizationConfig(
    @Autowired private val dataSource: DataSource,
    @Autowired private val passwordEncoder: PasswordEncoder,
    @Autowired private val authenticationManager: AuthenticationManager,
    @Autowired private val userDetailsService: UserDetailsService,
    @Autowired private val tokenStore: TokenStore,
    @Autowired private val jwtAccessTokenConverter: JwtAccessTokenConverter
) : AuthorizationServerConfigurerAdapter() {
    override fun configure(clients: ClientDetailsServiceConfigurer) {
        // {noop} 前缀告诉 DelegatingPasswordEncoder
        // 不使用密码加密比对，不加会报错
        // authorizedGrantTypes("refresh_token", "password", "client_credentials")
        // scopes("webclient", "mobileclient")
        clients.jdbc(dataSource)
            .passwordEncoder(passwordEncoder)
    }

    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {
        val tokenEnhancerChain = TokenEnhancerChain()
        tokenEnhancerChain.setTokenEnhancers(listOf(jwtAccessTokenConverter))
        endpoints.tokenStore(tokenStore)
            .accessTokenConverter(jwtAccessTokenConverter)
            .tokenEnhancer(tokenEnhancerChain)
            .authenticationManager(authenticationManager)
            .userDetailsService(userDetailsService)
    }

    @Bean
    fun jdbcClientDetailsService(): JdbcClientDetailsService {
        return JdbcClientDetailsService(dataSource)
    }
}