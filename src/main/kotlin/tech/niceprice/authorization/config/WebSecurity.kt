package tech.niceprice.authorization.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import javax.sql.DataSource

/**
 * 配置类
 * 管理 Spring Security
 * 内含用户名、密码、授权、密码加密解密方式等
 */
@Configuration
@EnableWebSecurity
class WebSecurity(
    @Autowired private val dataSource: DataSource,
    @Autowired private val passwordEncoder: PasswordEncoder
) : WebSecurityConfigurerAdapter() {
    @Bean override fun authenticationManagerBean(): AuthenticationManager =
        super.authenticationManagerBean()

    @Bean @Primary override fun userDetailsServiceBean(): UserDetailsService =
        super.userDetailsServiceBean()

    override fun configure(auth: AuthenticationManagerBuilder) {
        // 注意：roles 是 authorities 的简写形式
        // 前者省略 ROLE_ 但是方法保护时不可省略
        auth.jdbcAuthentication()
            .dataSource(dataSource)
            .passwordEncoder(passwordEncoder)
            .groupAuthoritiesByUsername(
                "SELECT g.id, g.group_name, ga.authority " +
                    "FROM group_members gm " +
                    "INNER JOIN groups g ON gm.group_id = g.id " +
                    "INNER JOIN group_authorities ga ON ga.group_id = g.id " +
                    "WHERE gm.username = ?")
    }
}