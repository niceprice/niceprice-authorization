package tech.niceprice.authorization.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import org.springframework.security.oauth2.provider.token.TokenStore

@Configuration
@EnableResourceServer
class ResourceConfig(
    @Value(value = "\${spring.application.name}")
    private val applicationName: String,
    @Autowired private val tokenStore: TokenStore
): ResourceServerConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        http.authorizeRequests()
            .antMatchers("/user/**").hasRole("CLIENT")
            .antMatchers("/group/**").hasRole("CLIENT")
            .anyRequest().permitAll()
            .and().csrf().disable()
    }

    override fun configure(resources: ResourceServerSecurityConfigurer) {
        resources.resourceId(applicationName)
            .tokenStore(tokenStore)
    }
}