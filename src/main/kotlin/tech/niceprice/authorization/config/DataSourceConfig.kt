package tech.niceprice.authorization.config

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction
import org.mybatis.spring.annotation.MapperScan
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.annotation.EnableTransactionManagement

/**
 * 配置数据源的配置类
 */
@Configuration
@EnableTransactionManagement  // 注释声明开启事务管理
@EnableDistributedTransaction // 开启全局分布式事务
@MapperScan(value = ["tech.niceprice.authorization.dao"]) // 声明包扫描
class DataSourceConfig