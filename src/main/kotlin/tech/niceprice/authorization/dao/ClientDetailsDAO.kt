package tech.niceprice.authorization.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional
import tech.niceprice.authorization.entity.SystemClientDetails

@Repository
@Transactional(isolation = Isolation.READ_COMMITTED)
class ClientDetailsDAO(
    @Autowired private val detailsService: JdbcClientDetailsService
) {
    fun loadClientByClientId(clientId: String) {
        detailsService.loadClientByClientId(clientId)
    }

    fun addClientDetails(details: SystemClientDetails) {
        detailsService.addClientDetails(details)
    }

    fun updateClientDetails(details: SystemClientDetails) {
        detailsService.updateClientDetails(details)
    }

    fun  updateClientSecret(clientId: String, secret: String) {
        detailsService.updateClientSecret(clientId, secret)
    }

    fun removeClientDetails(clientId: String) {
        detailsService.removeClientDetails(clientId)
    }
}