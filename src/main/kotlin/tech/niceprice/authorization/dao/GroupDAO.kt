package tech.niceprice.authorization.dao

import org.apache.ibatis.annotations.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional
import tech.niceprice.authorization.entity.GroupDO


@Repository
@Transactional(isolation = Isolation.READ_COMMITTED)
interface GroupDAO {
    fun getGroup(id: Long): GroupDO?

    fun updateGroupAuthorities(
        @Param(value = "id") id: Long,
        @Param(value = "inserts") inserts: List<String>,
        @Param(value = "deletes") deletes: List<String>
    ): Int

    fun updateGroupMembers(
        @Param(value = "id") id: Long,
        @Param(value = "inserts") inserts: List<Long>,
        @Param(value = "deletes") deletes: List<Long>
    ): Int

    fun updateGroupName(
        @Param(value = "id") id: Long,
        @Param(value = "name") name: String
    ): Int

    fun insertGroup(group: GroupDO): Int

    fun deleteGroup(id: Long): Int

    fun getAuthorities(id: Long): List<String>

    fun getMembers(id: Long): List<Long>
}