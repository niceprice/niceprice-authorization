package tech.niceprice.authorization.dao

import org.apache.ibatis.annotations.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional
import tech.niceprice.authorization.entity.UserDO
import java.util.*


@Repository
@Transactional(isolation = Isolation.READ_COMMITTED)
interface UserDAO  {
    fun getUser(username: Long): UserDO?

    fun updateUserAuthorities(
        @Param(value = "username") username: Long,
        @Param(value = "inserts") inserts: List<String>,
        @Param(value = "deletes") deletes: List<String>
    ): Int

    fun updateUserPassword(
        @Param(value = "username") username: Long,
        @Param(value = "password") password: String
    ): Int

    fun updateUserEnabled(
        @Param(value = "username") username: Long,
        @Param(value = "enabled") enabled: Boolean
    ): Int

    fun insertUser(user: UserDO): Int

    fun delete(username: Long): Int

    fun getAuthorities(username: Long): List<String>
}