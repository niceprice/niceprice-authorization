package tech.niceprice.authorization

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
@EnableDiscoveryClient
class NicePriceAuthorizationApplication

fun main(args: Array<String>) {
    runApplication<NicePriceAuthorizationApplication>(*args)
}
