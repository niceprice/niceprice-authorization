package tech.niceprice.authorization.controller.util

data class InsertsDeletesUpdateDO<T>(
    val id: Long = 0,
    val inserts: List<T> = listOf(),
    val deletes: List<T> = listOf()
)
