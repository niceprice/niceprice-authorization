@file:JvmName("ControllerUtil")
package tech.niceprice.authorization.controller.util


/**
 * 比对找出新旧两个集合中，新增的和删除的
 * @param old 旧集合
 * @param new 新集合
 * @return Tuple2 类型
 * 第一个为 inserts 即相比于旧新增的
 * 第二个为 deletes 即相比于新被删除的
 */
fun <T> compare(old: List<T>, new: List<T>): Pair<List<T>, List<T>> {
    val oldIsNotEmpty = old.isNotEmpty()
    val newIsNotEmpty = new.isNotEmpty()
    if (oldIsNotEmpty && newIsNotEmpty) {
        // 新的有，旧的没有 就是新增的
        val inserts = new.filter { item -> !old.contains(item) }.toList()
        // 旧的有，新的没有 就是删除的
        val deletes = old.filter { item -> !new.contains(item) }.toList()
        return Pair(inserts, deletes)
    } else if (!oldIsNotEmpty && newIsNotEmpty) {
        // 旧的空，全为新增
        return Pair(new, listOf())
    } else if (oldIsNotEmpty && !newIsNotEmpty) {
        // 新为空，全为删除
        return Pair(listOf(), old)
    } else if (!oldIsNotEmpty && !newIsNotEmpty) {
        return Pair(listOf(), listOf())
    }
    return Pair(listOf(), listOf())
}