package tech.niceprice.authorization.controller

import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping(value = ["/oauth2"])
class OAuth2UserController {
    @PostMapping(value = ["/user"])
    fun user(user: OAuth2Authentication): Map<String, Any> {
        val userInfo: MutableMap<String, Any> = HashMap()
        val auth: Authentication? = user.userAuthentication
        if (auth != null) {
            userInfo["user"] = auth.principal
            userInfo["authorities"] = AuthorityUtils.authorityListToSet(auth.authorities)
        } else {
            userInfo["message"] = "it is not a user, it is a client"
        }
        return userInfo
    }
}