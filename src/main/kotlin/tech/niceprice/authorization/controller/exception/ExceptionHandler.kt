package tech.niceprice.authorization.controller.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus

@ControllerAdvice
class ExceptionHandler {
    @ExceptionHandler(FieldsErrorException::class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    fun error(ex: FieldsErrorException): String = ex.exMessage

    @ExceptionHandler(FieldsNullException::class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    fun nil(ex: FieldsNullException): String = ex.exMessage

    @ExceptionHandler(GetNullException::class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    fun getNull(ex: GetNullException): String = ex.exMessage

    @ExceptionHandler(NullPointerException::class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    fun argNull(ex: NullPointerException): String = ex.message ?: "NullPointerException"

    @ExceptionHandler(IllegalArgumentException::class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    fun arg(ex: IllegalArgumentException): String = ex.message ?: "IllegalArgumentException"

    @ExceptionHandler(Exception::class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    fun exception(ex: Exception): String = ex.message ?: "Exception"
}