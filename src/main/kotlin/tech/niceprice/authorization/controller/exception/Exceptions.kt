package tech.niceprice.authorization.controller.exception

class FieldsErrorException(
    val exMessage: String
): RuntimeException("some fields may cause errors in your bean: cause by $exMessage")

class FieldsNullException(
    val exMessage: String
): RuntimeException("some fields in params is null: cause by $exMessage")

class GetNullException (
    val exMessage: String
): RuntimeException("get null: cause by $exMessage")