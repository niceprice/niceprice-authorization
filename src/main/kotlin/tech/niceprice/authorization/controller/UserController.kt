package tech.niceprice.authorization.controller

import com.codingapi.txlcn.tc.annotation.DTXPropagation
import com.codingapi.txlcn.tc.annotation.LcnTransaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.web.bind.annotation.*
import tech.niceprice.authorization.controller.util.InsertsDeletesUpdateDO
import tech.niceprice.authorization.controller.exception.FieldsErrorException
import tech.niceprice.authorization.controller.exception.FieldsNullException
import tech.niceprice.authorization.controller.exception.GetNullException
import tech.niceprice.authorization.controller.util.compare
import tech.niceprice.authorization.dao.UserDAO
import tech.niceprice.authorization.entity.UserDO
import tech.niceprice.authorization.snowflake.SnowFlakeWorker

@RestController
@RequestMapping(value = ["/user"])
class UserController(
    @Autowired private val userDAO: UserDAO,
    @Autowired private val snowFlakeWorker: SnowFlakeWorker,
    @Autowired private val passwordEncoder: PasswordEncoder
) {
    @GetMapping(value = ["/{username}"])
    @ResponseStatus(code = HttpStatus.OK)
    fun get(
        @PathVariable(value = "username") username: Long
    ): UserDO = userDAO.getUser(username) ?: throw GetNullException("get user")

    @PostMapping(value = ["/{username}"])
    @ResponseStatus(code = HttpStatus.CREATED)
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    fun insert(
        @RequestBody user: UserDO,
        @PathVariable(value = "username") username: Long,
        @RequestParam(value = "hasUsername") hasUsername: Boolean = true,
        @RequestParam(value = "isEncoded") isEncoded: Boolean = true
    ) {
        check(value = username == user.username) {
            throw FieldsErrorException("usernames are not same")
        }
        check(value = user.password.isNotBlank()) {
            throw FieldsNullException("password is null or blank")
        }
        if (!hasUsername || user.username <= 0) generateUserUsername(user)
        if (!isEncoded) encodeUserPassword(user)
        userDAO.insertUser(user)
    }

    @PutMapping(value = ["/{username}"])
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    fun update(
        @RequestBody user: UserDO,
        @PathVariable(value = "username") username: Long,
        @RequestParam(value = "isEncoded") isEncoded: Boolean = true,
        @RequestParam(value = "updateAuth") updateAuth: Boolean = true
    ) {
        check(value = username == user.username) {
            throw FieldsErrorException("usernames are not same")
        }
        val oldUser: UserDO = userDAO.getUser(user.username) ?: throw GetNullException("get user")
        // 更新 enabled
        if (oldUser.enabled != user.enabled) userDAO.updateUserEnabled(user.username, user.enabled)
        // 更新 password
        if (user.password.isNotBlank()) {
            if (isEncoded) {
                userDAO.updateUserPassword(user.username, user.password)
            } else {
                encodeUserPassword(user)
                userDAO.updateUserPassword(user.username, user.password)
            }
        }
        // 更新 authorities
        if (updateAuth) {
            val pair = compare(oldUser.authorities, user.authorities)
            userDAO.updateUserAuthorities(user.username, pair.first, pair.second)
        }
    }

    @PutMapping(value = ["/{username}/password"])
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    fun updatePassword(
        @RequestBody user: UserDO,
        @PathVariable(value = "username") username: Long,
        @RequestParam(value = "isEncoded") isEncoded: Boolean = true,
        userAuth: OAuth2Authentication
    ) {
        check(value = username == user.username &&
            username == userAuth.principal.toString().toLong()
        ) { throw FieldsErrorException("usernames are not same") }
        if (user.password.isNotBlank()) {
            if (isEncoded) {
                userDAO.updateUserPassword(user.username, user.password)
            } else {
                encodeUserPassword(user)
                userDAO.updateUserPassword(user.username, user.password)
            }
        }
    }

    @PutMapping(value = ["/{username}/authority"])
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    fun updateAuthorities(
        @RequestBody authorities: InsertsDeletesUpdateDO<String>,
        @PathVariable(value = "username") username: Long
    ) {
        check(value = username == authorities.id) {
            throw FieldsErrorException("usernames are not same")
        }
        userDAO.updateUserAuthorities(
            authorities.id, authorities.inserts, authorities.deletes)
    }

    @DeleteMapping(value = ["/{username}"])
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    fun delete(
        @PathVariable(value = "username") username: Long
    ) {
        val i: Int = userDAO.delete(username)
        if (i <= 0) throw FieldsErrorException("delete user is null")
    }

    private fun encodeUserPassword(user: UserDO) {
        val encodedPassword: String = passwordEncoder.encode(user.password)
        user.password = encodedPassword
    }

    private fun generateUserUsername(user: UserDO) {
        val username: Long = snowFlakeWorker.nextId()
        user.username = username
    }
}