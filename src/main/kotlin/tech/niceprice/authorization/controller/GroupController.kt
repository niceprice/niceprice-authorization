package tech.niceprice.authorization.controller

import com.codingapi.txlcn.tc.annotation.DTXPropagation
import com.codingapi.txlcn.tc.annotation.LcnTransaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import tech.niceprice.authorization.controller.exception.FieldsErrorException
import tech.niceprice.authorization.controller.exception.FieldsNullException
import tech.niceprice.authorization.controller.exception.GetNullException
import tech.niceprice.authorization.controller.util.InsertsDeletesUpdateDO
import tech.niceprice.authorization.controller.util.compare
import tech.niceprice.authorization.dao.GroupDAO
import tech.niceprice.authorization.entity.GroupDO

@RestController
@RequestMapping(value = ["/group"])
class GroupController(
    @Autowired private val groupDAO: GroupDAO
) {
    @GetMapping(value = ["/{groupId}"])
    @ResponseStatus(code = HttpStatus.OK)
    fun get(
        @PathVariable(value = "groupId") groupId: Long
    ): GroupDO = groupDAO.getGroup(groupId) ?: throw GetNullException("get group")

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    fun insert(
        @RequestBody group: GroupDO
    ) {
        check(value = group.groupName.isNotBlank()) {
            throw FieldsNullException("group name is null or blank")
        }
        groupDAO.insertGroup(group)
    }

    @PutMapping(value = ["/{groupId}"])
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    fun update(
        @RequestBody group: GroupDO,
        @PathVariable(value = "groupId") groupId: Long,
        @RequestParam(value = "updateAuth") updateAuth: Boolean = true,
        @RequestParam(value = "updateMember") updateMember: Boolean = true
    ) {
        check(value = groupId == group.id) {
            throw FieldsErrorException("group id are not same")
        }
        val oldGroup: GroupDO = groupDAO.getGroup(group.id) ?: throw GetNullException("get group")
        // 更新 group name
        if (oldGroup.groupName != group.groupName) groupDAO.updateGroupName(group.id, group.groupName)
        // 更新 authorities
        if (updateAuth) {
            val pair = compare(oldGroup.authorities, group.authorities)
            groupDAO.updateGroupAuthorities(group.id, pair.first, pair.second)
        }
        // 更新 members
        if (updateMember) {
            val pair = compare(oldGroup.members, group.members)
            groupDAO.updateGroupMembers(group.id, pair.first, pair.second)
        }
    }

    @PutMapping(value = ["/{groupId}/authority"])
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    fun incrementUpdateAuthorities(
        @RequestBody authorities: InsertsDeletesUpdateDO<String>,
        @PathVariable(value = "groupId") groupId: Long
    ) {
        check(value = groupId == authorities.id) {
            throw FieldsErrorException("group id are not same")
        }
        groupDAO.updateGroupAuthorities(
            authorities.id, authorities.inserts, authorities.deletes)
    }

    @PutMapping(value = ["/{groupId}/members"])
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    fun incrementUpdateMembers(
        @RequestBody members: InsertsDeletesUpdateDO<Long>,
        @PathVariable(value = "groupId") groupId: Long
    ) {
        check(value = groupId == members.id) {
            throw FieldsErrorException("group id are not same")
        }
        groupDAO.updateGroupMembers(
            members.id, members.inserts, members.deletes)
    }

    @DeleteMapping(value = ["/{groupId}"])
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    fun delete(
        @PathVariable(value = "groupId") groupId: Long
    ) {
        val i: Int = groupDAO.deleteGroup(groupId)
        if (i <= 0) throw FieldsErrorException("delete group is null")
    }
}