package tech.niceprice.authorization

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.Rollback
import tech.niceprice.authorization.controller.UserController
import tech.niceprice.authorization.dao.UserDAO
import tech.niceprice.authorization.entity.UserDO

@SpringBootTest
class UserTest {
    @Autowired lateinit var userDAO: UserDAO
    @Autowired lateinit var userController: UserController

    @Test
    fun testNil() {
        val user = userDAO.getUser(1234)
        println(user ?: UserDO())
        println(user!!.authorities == null)
        println(user!!.authorities.size)
    }

    @Test
    fun testController() {
        println(userController.get(1234))
        println(userController.get(123))
    }

    fun test() {
        val user = UserDO(
            10001000,
            "{noop}1234",
            listOf("ROLE_USER")
        )
        println("insert effects ${userDAO.insertUser(user)} rows")
        println("inserted user ${userDAO.getUser(user.username)}")
        user.password = "1234"
        println("update password effects ${userDAO.updateUserPassword(user.username,user.password)}")
        println("updated password ${userDAO.getUser(user.username)}")
        user.enabled = false
        println("update enabled effects ${userDAO.updateUserEnabled(user.username,false)}")
        println("updated enabled ${userDAO.getUser(user.username)}")

        println("update auths effects " +
            "${userDAO.updateUserAuthorities(user.username, 
                listOf("ROLE_CLIENT?","ROLE_ADMIN"), listOf())}")
        println("updated auths ${userDAO.getUser(user.username)}")

        println("update auths2 effects " +
            "${userDAO.updateUserAuthorities(user.username,
                listOf(),listOf("ROLE_CLIENT?","ROLE_ADMIN"))}")
        println("updated auths2 ${userDAO.getUser(user.username)}")

        println("update auths3 effects " +
            "${userDAO.updateUserAuthorities(user.username,
                listOf("ROLE_CLIENT"),listOf("ROLE_USER"))}")
        println("updated auths3 ${userDAO.getUser(user.username)}")

        println("delete user effects ${userDAO.delete(user.username)}")
        println("deleted user ${userDAO.getUser(user.username)}")
    }
}