package tech.niceprice.authorization

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails
import org.springframework.security.oauth2.provider.client.BaseClientDetails
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService

@SpringBootTest
class BaseClientDetailsTest  {
    @Autowired lateinit var jdbcClientDetailsService: JdbcClientDetailsService

    @Test
    fun addClient() {
        val clientDetails = BaseClientDetails()

        clientDetails.clientId = "example"
        clientDetails.clientSecret = "{noop}example"
        clientDetails.authorities = setOf(SimpleGrantedAuthority("ROLE_CLIENT"))
        clientDetails.setResourceIds(setOf("authorization"))
        clientDetails.setAuthorizedGrantTypes(setOf("client_credentials","password"))
        clientDetails.setScope(setOf("all"))

        jdbcClientDetailsService.addClientDetails(clientDetails)
    }
}