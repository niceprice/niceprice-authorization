package tech.niceprice.authorization

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.Rollback
import tech.niceprice.authorization.dao.GroupDAO
import tech.niceprice.authorization.dao.UserDAO
import tech.niceprice.authorization.entity.GroupDO
import tech.niceprice.authorization.entity.UserDO

@SpringBootTest
class GroupDAOTest {
    @Autowired lateinit var userDAO: UserDAO
    @Autowired lateinit var groupDAO: GroupDAO

    @Test
    @Rollback
    fun test() {
        val group = GroupDO(
            0,
            "group",
            listOf("ROLE_GROUP")
        )
        println("insert effects ${groupDAO.insertGroup(group)} rows")
        println("inserted group ${groupDAO.getGroup(group.id)}")
        group.groupName = "group_name"
        println("update password effects ${groupDAO.updateGroupName(group.id,group.groupName)}")
        println("updated password ${groupDAO.getGroup(group.id)}")

        println("update auths effects " +
            "${groupDAO.updateGroupAuthorities(group.id,
                listOf("ROLE_CLIENT?","ROLE_ADMIN"), listOf())}")
        println("updated auths ${groupDAO.getGroup(group.id)}")

        println("update auths2 effects " +
            "${groupDAO.updateGroupAuthorities(group.id,
                listOf(),listOf("ROLE_CLIENT?","ROLE_ADMIN"))}")
        println("updated auths2 ${groupDAO.getGroup(group.id)}")

        println("update auths3 effects " +
            "${groupDAO.updateGroupAuthorities(group.id,
                listOf("ROLE_CLIENT"),listOf("ROLE_GROUP"))}")
        println("updated auths3 ${groupDAO.getGroup(group.id)}")

        val user = UserDO(10001000, "{noop}1234", listOf("ROLE_USER"))
        userDAO.insertUser(user)
        val user1 = UserDO(10001001, "{noop}1234", listOf("ROLE_USER"))
        userDAO.insertUser(user1)
        val user2 = UserDO(10001002, "{noop}1234", listOf("ROLE_USER"))
        userDAO.insertUser(user2)

        println("update members effects " +
            "${groupDAO.updateGroupMembers(group.id,
                listOf(user.username,user1.username,user2.username), listOf())}")
        println("updated members ${groupDAO.getMembers(group.id)}")

        println("update members2 effects " +
            "${groupDAO.updateGroupMembers(group.id,
                listOf(),listOf(user.username,user1.username))}")
        println("updated members2 ${groupDAO.getMembers(group.id)}")

        println("update members3 effects " +
            "${groupDAO.updateGroupMembers(group.id,
                listOf(user1.username),listOf(user2.username))}")
        println("updated members3 ${groupDAO.getMembers(group.id)}")


        println("delete group effects ${groupDAO.deleteGroup(group.id)}")
        println("deleted group ${groupDAO.getGroup(group.id)}")

        userDAO.delete(user.username)
        userDAO.delete(user1.username)
        userDAO.delete(user2.username)
    }
}