# Authorization

本项目为微服务架构中业务组件——身份认证和鉴权中心

NicePrice 为项目名称，如需改造中间件可忽略
***
##### 依赖
* 分布式事务中心 —— txlcn-tc （具体内容参见 [快速开始](https://www.txlcn.org/zh-cn/docs/start.html) ）
* 服务注册与发现中心 —— Consul
* 应用健康监控 —— SpringBoot Actuator
* 分布式链路追踪 —— sleuth & zipkin
* 关系型数据库 —— MySQL
***
##### 工作原理
* sql/users.sql —— [RBAC 表设计](https://docs.spring.io/spring-security/site/docs/3.0.x/reference/appendix-schema.html)

内含分组角色的权限控制

* sql/clients.sql —— [OAuth2 表设计](https://github.com/spring-projects/spring-security-oauth/blob/master/spring-security-oauth2/src/test/resources/schema.sql)

因系统中仅使用 Password 和 Client Credentials 两种认证模式，
故系统中只提供部分接口，且用例有限

* config/ResourceConfig —— controller 路径访问的权限配置

* controller/OAuth2UserController —— 系统内其他 client 验证 token

* controller/UserController —— 为系统内其他 client 提供用户信息服务

* controller/GroupController —— 为系统内其他 client 提供用户组信息服务
***
##### 注意
* 分布式事务
 
UserController、GroupController 各方法均有 Lcn 分布式事务注解，
如需启用仅在其他 client 调用处使用 @LcnTransaction(propagation = DTXPropagation.REQUIRED) 即可，
若无注解，各方法将仅开启本地事务，不新建分布式事务组

* 资源服务认证

UserController、GroupController 各方法调用需要权限认证，
该系统中默认各方法仅以 client 角色使用，
故其他 client 中 OAuth2RestTemplate 和 Openfeign 的配置仅需配置 client 属性
并注入 DefaultClientContext 让方法调用以 client 角色实行

如果需要自定义传递令牌，或者限制 client 访问 resource server ，
仅需修改 oauth_client_details 中各属性，可参见 [spring-security-oauth](https://github.com/spring-projects/spring-security-oauth)
或 [OAuth-2.0-Migration-Guide](https://github.com/spring-projects/spring-security/wiki/OAuth-2.0-Migration-Guide)