-- 以下表为 Spring Security 源码中 sql 反向推导得出
-- jdbcAuthentication() 方法中将使用以下表
-- 每表中 creation_time 和 modify_time 及其触发器均为审计设计

-- 用户表
CREATE TABLE users (
    username BIGINT UNSIGNED PRIMARY KEY,
    password VARCHAR(128) NOT NULL,
    enabled TINYINT(1) NOT NULL DEFAULT 1,
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER users_modify_time
BEFORE UPDATE ON users FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;


-- 权限表
CREATE TABLE authorities (
    username BIGINT UNSIGNED,
    authority VARCHAR(16) NOT NULL,
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY(username,authority),
    CONSTRAINT fk_auth_username FOREIGN KEY (username) REFERENCES users(username) ON DELETE CASCADE ON UPDATE CASCADE,
    INDEX idx_auth_username(username)
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER auth_modify_time
BEFORE UPDATE ON authorities FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;


-- 用户组表
CREATE TABLE groups (
    id MEDIUMINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    group_name VARCHAR(32) NOT NULL,
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    INDEX idx_groups_name(group_name)
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER groups_modify_time
BEFORE UPDATE ON groups FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;

-- ALTER TABLE groups MODIFY COLUMN group_name VARCHAR(32) NOT NULL;
-- ALTER TABLE groups DROP INDEX group_name;

-- 用户组权限表
CREATE TABLE group_authorities (
    group_id MEDIUMINT UNSIGNED,
    authority VARCHAR(16) NOT NULL,
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY(group_id,authority),
    CONSTRAINT fk_ga_group_id FOREIGN KEY (group_id) REFERENCES groups(id) ON DELETE CASCADE ON UPDATE CASCADE,
    INDEX idx_ga_id(group_id)
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER ga_modify_time
BEFORE UPDATE ON group_authorities FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;


-- 用户组组员
CREATE TABLE group_members (
    group_id MEDIUMINT UNSIGNED,
    username BIGINT UNSIGNED,
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY(group_id,username),
    CONSTRAINT fk_gm_group_id FOREIGN KEY (group_id) REFERENCES groups(id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_gm_username FOREIGN KEY (username) REFERENCES users(username) ON DELETE CASCADE ON UPDATE CASCADE,
    INDEX idx_gm_id(group_id)
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER gm_modify_time
BEFORE UPDATE ON group_members FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;

