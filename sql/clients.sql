-- 以下表为 Spring Security OAuth2 设计
-- 实现 client credentials 和 password 模式设计

CREATE TABLE oauth_client_details (
    client_id VARCHAR(32) PRIMARY KEY,
    client_secret VARCHAR(128),
    resource_ids VARCHAR(256),
    scope VARCHAR(64),
    authorized_grant_types VARCHAR(128),
    web_server_redirect_uri VARCHAR(256),
    authorities VARCHAR(128),
    access_token_validity INTEGER,
    refresh_token_validity INTEGER,
    additional_information BLOB,
    autoapprove VARCHAR(256),
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER ocd_modify_time
    BEFORE UPDATE ON oauth_client_details FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;

CREATE TABLE oauth_client_token (
    token_id VARCHAR(512),
    token BLOB,
    authentication_id VARCHAR(32) PRIMARY KEY,-- 用户对资源访问的情况
    user_name VARCHAR(32),
    client_id VARCHAR(32),
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER oct_modify_time
    BEFORE UPDATE ON oauth_client_token FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;


-- 该表用于在客户端系统中存储从服务端获取的token数据
CREATE TABLE oauth_access_token (
    token_id VARCHAR(512),
    token BLOB,
    authentication_id VARCHAR(32) PRIMARY KEY,
    user_name VARCHAR(32),
    client_id VARCHAR(32),
    authentication BLOB,
    refresh_token VARCHAR(512),
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    INDEX oat_token_id (token_id(32)),
    INDEX oat_user_name (user_name),
    INDEX oat_client_id (client_id)
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER oat_modify_time
    BEFORE UPDATE ON oauth_access_token FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;

-- 如果客户端的grant_type不支持refresh_token,则不会使用该表
CREATE TABLE oauth_refresh_token (
    token_id VARCHAR(512),
    token BLOB,
    authentication BLOB,
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    INDEX ort_token_id (token_id(32))
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER ort_modify_time
    BEFORE UPDATE ON oauth_refresh_token FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;


-- 只有当grant_type为"authorization_code"时
-- v该表中才会有数据产生
CREATE TABLE oauth_code (
    code VARCHAR(256),
    authentication BLOB,
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER oc_modify_time
    BEFORE UPDATE ON oauth_code FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;

CREATE TABLE oauth_approvals (
    userId VARCHAR(32),
    clientId VARCHAR(32),
    scope VARCHAR(64),
    status VARCHAR(10),
    expiresAt TIMESTAMP,
    lastModifiedAt TIMESTAMP,
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
-- UPDATE TRIGGER FOR SYN modify_time
DELIMITER /
CREATE TRIGGER oa_modify_time
    BEFORE UPDATE ON oauth_approvals FOR EACH ROW
BEGIN
    SET NEW.modify_time = CURRENT_TIMESTAMP;
END
/
DELIMITER ;

