FROM openjdk:13-alpine
VOLUME /tmp
ARG JAR_FILE

RUN apk --no-cache add tzdata  && \
    ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo "Asia/Shanghai" > /etc/timezone

COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]